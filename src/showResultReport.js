const { checkIsBuildEqual } = require('./commands/filesCommands');

const { bgBlue, bgGreen, green, red } = require('colorette');

const showResultReport = (initialBuildResult, buildResultToCompare) => {
  const isResultEqual = checkIsBuildEqual(initialBuildResult, buildResultToCompare);

  console.log('isResultEqual', isResultEqual);
  console.log('initialBuildReulst', initialBuildResult);

  if (isResultEqual) {
    const {
      sumStatSize, sumParsedSize, sumGzipSize,
      branchName, commitHash
    } = initialBuildResult;

    const { branchName: branchToCompare, commitHash: commitHashToCompare } = buildResultToCompare;

    console.log(bgGreen('Builds equal'));
    console.log(
      bgBlue(
        `=== Branch ${branchName} (${commitHash}) and ${branchToCompare} (${commitHashToCompare}) have the same bundle size ===`
      )
    );

    console.log('Stat size: ', green(sumStatSize));
    console.log('Parsed size: ', green(sumParsedSize));
    console.log('Gzip size: ', green(sumGzipSize));
  } else {
    const {
      branchName, commitHash
    } = initialBuildResult;

    const {
      branchName: branchToCompare,
      commitHash: commitHashToCompare,
    } = buildResultToCompare;

    const isInitialBranchSmaller = initialBuildResult.sumStatSize < buildResultToCompare.sumStatSize;

    console.log(
      bgBlue(`==== Initial branch: ${branchName} === ${commitHash} ====`)
    );

    const initialBranchResultColor = (text) => (isInitialBranchSmaller ? green(text) : red(text));
    const comparedBranchResultColor = (text) =>
      isInitialBranchSmaller ? red(text) : green(text);

    console.log('Stat size: ', initialBranchResultColor(initialBuildResult.sumStatSize));
    console.log('Parsed size: ', initialBranchResultColor(initialBuildResult.sumParsedSize));
    console.log('Gzip size: ', initialBranchResultColor(initialBuildResult.sumGzipSize));

    console.log(
      bgBlue(`==== Branch to compare: ${branchToCompare} === ${commitHashToCompare} ====`)
    );

    console.log('Stat size: ', comparedBranchResultColor(buildResultToCompare.sumStatSize));
    console.log('Parsed size: ', comparedBranchResultColor(buildResultToCompare.sumParsedSize));
    console.log('Gzip size: ', comparedBranchResultColor(buildResultToCompare.sumGzipSize));
  }
};

module.exports = {
  showResultReport,
};
