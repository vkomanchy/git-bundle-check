// --init - not required - hash or branch
// --compare - required - hash or branch
// --command - required - npm script name
// --reinstall - not required - indicate if package will install packages before builds

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');

const getSettingsFromArgs = (commands) => {
  const argv = yargs(hideBin(process.argv))
    .alias({
      i: 'init',
      r: 'reinstall',
      c: 'compare',
      cmd: 'command',
      h: 'help',
    })
    .describe({
      i: 'Initial branch or hash on which script will start work',
      c: 'Branch or hash with which script will compare initial branch build',
      cmd: 'Command with which build should be runned',
      r: 'Indicate if installing packages should be triggered after changing branches',
    })
    .demandOption(['c','cmd'])
    .help('h')
    .choices('cmd', commands)
    .check((argvForCheck) => {
      if (!argvForCheck.compare || argvForCheck.compare.length === 0) {
        throw new Error('Compare branch or hash empty! Please provide valid branch name or hash');
      }

      if (!argvForCheck.command) {
        throw new Error('Command for build is not valid');
      }

      return true;
    })
    .usage('$0 --i [branch name] --c [branch name] --cmd [command name] --r')
    .version()
    .argv;

  const initialBranchOrHash = argv.init;

  const branchOrHashToCheck = argv.compare;

  const commandToBuild = argv.command;

  const shouldInstallPackages = argv.reinstall;

  return {
    initialBranchOrHash,
    branchOrHashToCheck,
    commandToBuild,

    shouldInstallPackages,
  };
};

module.exports = { getSettingsFromArgs };
