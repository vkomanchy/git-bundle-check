const util = require('util');
const exec = util.promisify(require('child_process').exec);

const { green, red } = require('colorette');

const getCommitHash = async () => {
  try {
    const { stdout: commitHash, stderr } = await exec('git rev-parse --short HEAD');

    if (stderr) throw new Error(stderr);

    return commitHash.trim();
  } catch (error) {
    console.error(red('Error while check git commit hash'), error);
  }
};

const getBranchName = async () => {
  try {
    const { stdout: currentBranch, stderr } = await exec('git rev-parse --abbrev-ref HEAD');

    if (stderr) throw new Error(stderr);

    return currentBranch.replace('*', '').trim();
  } catch (error) {
    console.error(red('Error while check git branch'));
  }
};

const changeHEAD = async (branchNameOrHash) => {
  try {
    const data = await exec(`git checkout ${branchNameOrHash}`);

    if (data.stderr) throw new Error(data.stderr);

    console.log(green(`HEAD changed to ${branchNameOrHash}`))

  } catch (error) {
    console.error(red('Error while changing branch or hash'), error);
  }
};

module.exports = {
  getCommitHash,
  getBranchName,
  changeHEAD,
};
