const util = require('util');
const fs = require('fs');
const fsPromisified = require('fs/promises');
const { getBranchName, getCommitHash } = require('./gitCommands');

const { green, red } = require('colorette');

const exec = util.promisify(require('child_process').exec);

const installPackages = async () => {
  try {
    const isYarnInstalled = fs.existsSync('yarn.lock');

    const { stderr } = await exec(isYarnInstalled ? 'yarn' : 'npm i');

    if (stderr) throw new Error(stderr);

    console.log(green('Packages installed'));
  } catch (error) {
    console.error(red('Error while installing packages'), error);
  }
};

const convertBytes = (bytes) => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

  if (bytes == 0) {
    return 'n/a';
  }

  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

  if (i == 0) {
    return bytes + ' ' + sizes[i];
  }

  return `${(bytes / Math.pow(1024, i)).toFixed(3)} ${sizes[i]}`;
};

const runBuild = async (buildCommand, shouldInstallPackages) => {
  try {
    if (shouldInstallPackages) {
      await installPackages();
    }

    const { stderr } = await exec(`npm run ${buildCommand}`);

    if (stderr) throw new Error(stderr);

    const data = await fsPromisified.readFile('./bundle-sizes-info.json', 'utf8');

    const jsonData = JSON.parse(data);

    const getSumSize = (prop) =>
      jsonData.reduce((acc, current) => {
        acc += current[prop];

        return acc;
      }, 0);

    const sumStatSizeInBytes = getSumSize('statSize');
    const sumParsedSizeInBytes = getSumSize('parsedSize');
    const sumGzipSizeInBytes = getSumSize('gzipSize');

    const commitHash = await getCommitHash();
    const branchName = await getBranchName();

    return {
      sumStatSize: convertBytes(sumStatSizeInBytes),
      sumParsedSize: convertBytes(sumParsedSizeInBytes),
      sumGzipSize: convertBytes(sumGzipSizeInBytes),

      commitHash,
      branchName,
    };
  } catch (error) {
    console.error(red('Error while running build'), error);
  }
};

const checkIsBuildEqual = (startResult, endResult) => {
  const { sumStatSize, sumParsedSize, sumGzipSize } = startResult;

  const {
    sumStatSize: endSumStatSize,
    sumParsedSize: endSumParsedSize,
    sumGzipSize: endSumGzipSize,
  } = endResult;

  return (
    sumStatSize === endSumStatSize &&
    sumParsedSize === endSumParsedSize &&
    sumGzipSize === endSumGzipSize
  );
};

module.exports = {
  installPackages,
  runBuild,
  checkIsBuildEqual,
};
