#!/usr/bin/env node

const fsPromisified = require('fs/promises');
const process = require('process');

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;

const { getBranchName, changeHEAD } = require('./commands/gitCommands');
const { runBuild } = require('./commands/filesCommands');

const inquirer = require('inquirer');
const { showResultReport } = require('./showResultReport');
const { getSettingsFromArgs } = require('./getSettingsFromArgs');

const MODES = {
  inline: 'inline',
  prompt: 'prompt',
};

const runScript = async () => {
  try {
    const initBranch = await getBranchName();

    const rawData = await fsPromisified.readFile('package.json');
    const { scripts } = JSON.parse(rawData);

    const commands = Object.keys(scripts);

    const userPrompts = [
      {
        type: 'input',
        name: 'initialBranchOrHash',
        message: 'Choose first branch or hash to check:',
        default: initBranch,
      },
      {
        type: 'input',
        name: 'branchOrHashToCheck',
        message: 'Choose branch or hash to compare with initial:',
      },
      {
        type: 'list',
        name: 'commandToBuild',
        message:
          'Choose command to build with webpack-bundle-analyzer. WARNING: Current version compatible only with json as analyzerMode',
        choices: commands,
      },
      {
        type: 'confirm',
        name: 'shouldInstallPackages',
        message: 'Should packages be installed before each build?',
      },
    ];

    let MODE = 'prompt';

    if (Object.keys(argv) && Object.keys(argv).length > 0) {
      MODE = MODES.inline;
    }

    const compareSettings =
      MODE === MODES.prompt ? await inquirer.prompt(userPrompts) : await getSettingsFromArgs(commands);

    console.log('compareSettings', compareSettings);

    const {
      initialBranchOrHash = initBranch,
      branchOrHashToCheck,
      commandToBuild,
      shouldInstallPackages,
    } = compareSettings;

    if (initBranch !== initialBranchOrHash) {
      await changeHEAD(initialBranchOrHash);
    }

    const initialBuildResult = await runBuild(commandToBuild, shouldInstallPackages);

    await changeHEAD(branchOrHashToCheck);

    const buildResultToCompare = await runBuild(commandToBuild, shouldInstallPackages);

    showResultReport(initialBuildResult, buildResultToCompare);
  } catch (error) {
    console.error('Unhandled error', error);
  } finally {
    await changeHEAD(initBranch);
  }
};

runScript();
