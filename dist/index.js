#!/usr/bin/env node
var $gXNCa$fspromises = require("fs/promises");
var $gXNCa$inquirer = require("inquirer");
var $gXNCa$process = require("process");
var $gXNCa$yargsyargs = require("yargs/yargs");
var $gXNCa$yargshelpers = require("yargs/helpers");
var $gXNCa$util = require("util");
var $gXNCa$child_process = require("child_process");
var $gXNCa$colorette = require("colorette");
var $gXNCa$fs = require("fs");





var $4fa36e821943b400$require$hideBin = $gXNCa$yargshelpers.hideBin;
const $4fa36e821943b400$var$argv = $gXNCa$yargsyargs($4fa36e821943b400$require$hideBin($gXNCa$process.argv)).argv;
var $e3deae093bde0d3f$exports = {};


const $e3deae093bde0d3f$var$exec = $gXNCa$util.promisify($gXNCa$child_process.exec);

var $e3deae093bde0d3f$require$green = $gXNCa$colorette.green;
var $e3deae093bde0d3f$require$red = $gXNCa$colorette.red;
const $e3deae093bde0d3f$var$getCommitHash = async ()=>{
    try {
        const { stdout: commitHash , stderr: stderr  } = await $e3deae093bde0d3f$var$exec("git rev-parse --short HEAD");
        if (stderr) throw new Error(stderr);
        return commitHash.trim();
    } catch (error) {
        console.error($e3deae093bde0d3f$require$red("Error while check git commit hash"), error);
    }
};
const $e3deae093bde0d3f$var$getBranchName = async ()=>{
    try {
        const { stdout: currentBranch , stderr: stderr  } = await $e3deae093bde0d3f$var$exec("git rev-parse --abbrev-ref HEAD");
        if (stderr) throw new Error(stderr);
        return currentBranch.replace("*", "").trim();
    } catch (error) {
        console.error($e3deae093bde0d3f$require$red("Error while check git branch"));
    }
};
const $e3deae093bde0d3f$var$changeHEAD = async (branchNameOrHash)=>{
    try {
        const data = await $e3deae093bde0d3f$var$exec(`git checkout ${branchNameOrHash}`);
        if (data.stderr) throw new Error(data.stderr);
        console.log($e3deae093bde0d3f$require$green(`HEAD changed to ${branchNameOrHash}`));
    } catch (error) {
        console.error($e3deae093bde0d3f$require$red("Error while changing branch or hash"), error);
    }
};
$e3deae093bde0d3f$exports = {
    getCommitHash: $e3deae093bde0d3f$var$getCommitHash,
    getBranchName: $e3deae093bde0d3f$var$getBranchName,
    changeHEAD: $e3deae093bde0d3f$var$changeHEAD
};


var $4fa36e821943b400$require$getBranchName = $e3deae093bde0d3f$exports.getBranchName;
var $4fa36e821943b400$require$changeHEAD = $e3deae093bde0d3f$exports.changeHEAD;
var $cf88d3e76500b922$exports = {};




var $cf88d3e76500b922$require$getBranchName = $e3deae093bde0d3f$exports.getBranchName;
var $cf88d3e76500b922$require$getCommitHash = $e3deae093bde0d3f$exports.getCommitHash;

var $cf88d3e76500b922$require$green = $gXNCa$colorette.green;
var $cf88d3e76500b922$require$red = $gXNCa$colorette.red;

const $cf88d3e76500b922$var$exec = $gXNCa$util.promisify($gXNCa$child_process.exec);
const $cf88d3e76500b922$var$installPackages = async ()=>{
    try {
        const isYarnInstalled = $gXNCa$fs.existsSync("yarn.lock");
        const { stderr: stderr  } = await $cf88d3e76500b922$var$exec(isYarnInstalled ? "yarn" : "npm i");
        if (stderr) throw new Error(stderr);
        console.log($cf88d3e76500b922$require$green("Packages installed"));
    } catch (error) {
        console.error($cf88d3e76500b922$require$red("Error while installing packages"), error);
    }
};
const $cf88d3e76500b922$var$convertBytes = (bytes)=>{
    const sizes = [
        "Bytes",
        "KB",
        "MB",
        "GB",
        "TB"
    ];
    if (bytes == 0) return "n/a";
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + " " + sizes[i];
    return `${(bytes / Math.pow(1024, i)).toFixed(3)} ${sizes[i]}`;
};
const $cf88d3e76500b922$var$runBuild = async (buildCommand, shouldInstallPackages)=>{
    try {
        if (shouldInstallPackages) await $cf88d3e76500b922$var$installPackages();
        const { stderr: stderr  } = await $cf88d3e76500b922$var$exec(`npm run ${buildCommand}`);
        if (stderr) throw new Error(stderr);
        const data = await $gXNCa$fspromises.readFile("./bundle-sizes-info.json", "utf8");
        const jsonData = JSON.parse(data);
        const getSumSize = (prop)=>jsonData.reduce((acc, current)=>{
                acc += current[prop];
                return acc;
            }, 0);
        const sumStatSizeInBytes = getSumSize("statSize");
        const sumParsedSizeInBytes = getSumSize("parsedSize");
        const sumGzipSizeInBytes = getSumSize("gzipSize");
        const commitHash = await $cf88d3e76500b922$require$getCommitHash();
        const branchName = await $cf88d3e76500b922$require$getBranchName();
        return {
            sumStatSize: $cf88d3e76500b922$var$convertBytes(sumStatSizeInBytes),
            sumParsedSize: $cf88d3e76500b922$var$convertBytes(sumParsedSizeInBytes),
            sumGzipSize: $cf88d3e76500b922$var$convertBytes(sumGzipSizeInBytes),
            commitHash: commitHash,
            branchName: branchName
        };
    } catch (error) {
        console.error($cf88d3e76500b922$require$red("Error while running build"), error);
    }
};
const $cf88d3e76500b922$var$checkIsBuildEqual = (startResult, endResult)=>{
    const { sumStatSize: sumStatSize , sumParsedSize: sumParsedSize , sumGzipSize: sumGzipSize  } = startResult;
    const { sumStatSize: endSumStatSize , sumParsedSize: endSumParsedSize , sumGzipSize: endSumGzipSize ,  } = endResult;
    return sumStatSize === endSumStatSize && sumParsedSize === endSumParsedSize && sumGzipSize === endSumGzipSize;
};
$cf88d3e76500b922$exports = {
    installPackages: $cf88d3e76500b922$var$installPackages,
    runBuild: $cf88d3e76500b922$var$runBuild,
    checkIsBuildEqual: $cf88d3e76500b922$var$checkIsBuildEqual
};


var $4fa36e821943b400$require$runBuild = $cf88d3e76500b922$exports.runBuild;

var $70fa21487840dc56$exports = {};

var $70fa21487840dc56$require$checkIsBuildEqual = $cf88d3e76500b922$exports.checkIsBuildEqual;

var $70fa21487840dc56$require$bgBlue = $gXNCa$colorette.bgBlue;
var $70fa21487840dc56$require$bgGreen = $gXNCa$colorette.bgGreen;
var $70fa21487840dc56$require$green = $gXNCa$colorette.green;
var $70fa21487840dc56$require$red = $gXNCa$colorette.red;
const $70fa21487840dc56$var$showResultReport = (initialBuildResult, buildResultToCompare)=>{
    const isResultEqual = $70fa21487840dc56$require$checkIsBuildEqual(initialBuildResult, buildResultToCompare);
    console.log("isResultEqual", isResultEqual);
    console.log("initialBuildReulst", initialBuildResult);
    if (isResultEqual) {
        const { sumStatSize: sumStatSize , sumParsedSize: sumParsedSize , sumGzipSize: sumGzipSize , branchName: branchName , commitHash: commitHash  } = initialBuildResult;
        const { branchName: branchToCompare , commitHash: commitHashToCompare  } = buildResultToCompare;
        console.log($70fa21487840dc56$require$bgGreen("Builds equal"));
        console.log($70fa21487840dc56$require$bgBlue(`=== Branch ${branchName} (${commitHash}) and ${branchToCompare} (${commitHashToCompare}) have the same bundle size ===`));
        console.log("Stat size: ", $70fa21487840dc56$require$green(sumStatSize));
        console.log("Parsed size: ", $70fa21487840dc56$require$green(sumParsedSize));
        console.log("Gzip size: ", $70fa21487840dc56$require$green(sumGzipSize));
    } else {
        const { branchName: branchName1 , commitHash: commitHash1  } = initialBuildResult;
        const { branchName: branchToCompare1 , commitHash: commitHashToCompare1 ,  } = buildResultToCompare;
        const isInitialBranchSmaller = initialBuildResult.sumStatSize < buildResultToCompare.sumStatSize;
        console.log($70fa21487840dc56$require$bgBlue(`==== Initial branch: ${branchName1} === ${commitHash1} ====`));
        const initialBranchResultColor = (text)=>isInitialBranchSmaller ? $70fa21487840dc56$require$green(text) : $70fa21487840dc56$require$red(text);
        const comparedBranchResultColor = (text)=>isInitialBranchSmaller ? $70fa21487840dc56$require$red(text) : $70fa21487840dc56$require$green(text);
        console.log("Stat size: ", initialBranchResultColor(initialBuildResult.sumStatSize));
        console.log("Parsed size: ", initialBranchResultColor(initialBuildResult.sumParsedSize));
        console.log("Gzip size: ", initialBranchResultColor(initialBuildResult.sumGzipSize));
        console.log($70fa21487840dc56$require$bgBlue(`==== Branch to compare: ${branchToCompare1} === ${commitHashToCompare1} ====`));
        console.log("Stat size: ", comparedBranchResultColor(buildResultToCompare.sumStatSize));
        console.log("Parsed size: ", comparedBranchResultColor(buildResultToCompare.sumParsedSize));
        console.log("Gzip size: ", comparedBranchResultColor(buildResultToCompare.sumGzipSize));
    }
};
$70fa21487840dc56$exports = {
    showResultReport: $70fa21487840dc56$var$showResultReport
};


var $4fa36e821943b400$require$showResultReport = $70fa21487840dc56$exports.showResultReport;
var $c0b5daaf5dd84c0d$exports = {};



var $c0b5daaf5dd84c0d$require$hideBin = $gXNCa$yargshelpers.hideBin;
const $c0b5daaf5dd84c0d$var$getSettingsFromArgs = (commands)=>{
    const argv = $gXNCa$yargsyargs($c0b5daaf5dd84c0d$require$hideBin($gXNCa$process.argv)).alias({
        i: "init",
        r: "reinstall",
        c: "compare",
        cmd: "command",
        h: "help"
    }).describe({
        i: "Initial branch or hash on which script will start work",
        c: "Branch or hash with which script will compare initial branch build",
        cmd: "Command with which build should be runned",
        r: "Indicate if installing packages should be triggered after changing branches"
    }).demandOption([
        "c",
        "cmd"
    ]).help("h").choices("cmd", commands).check((argvForCheck)=>{
        if (!argvForCheck.compare || argvForCheck.compare.length === 0) throw new Error("Compare branch or hash empty! Please provide valid branch name or hash");
        if (!argvForCheck.command) throw new Error("Command for build is not valid");
        return true;
    }).usage("$0 --i [branch name] --c [branch name] --cmd [command name] --r").version().argv;
    const initialBranchOrHash = argv.init;
    const branchOrHashToCheck = argv.compare;
    const commandToBuild = argv.command;
    const shouldInstallPackages = argv.reinstall;
    return {
        initialBranchOrHash: initialBranchOrHash,
        branchOrHashToCheck: branchOrHashToCheck,
        commandToBuild: commandToBuild,
        shouldInstallPackages: shouldInstallPackages
    };
};
$c0b5daaf5dd84c0d$exports = {
    getSettingsFromArgs: $c0b5daaf5dd84c0d$var$getSettingsFromArgs
};


var $4fa36e821943b400$require$getSettingsFromArgs = $c0b5daaf5dd84c0d$exports.getSettingsFromArgs;
const $4fa36e821943b400$var$MODES = {
    inline: "inline",
    prompt: "prompt"
};
const $4fa36e821943b400$var$runScript = async ()=>{
    try {
        const initBranch1 = await $4fa36e821943b400$require$getBranchName();
        const rawData = await $gXNCa$fspromises.readFile("package.json");
        const { scripts: scripts  } = JSON.parse(rawData);
        const commands = Object.keys(scripts);
        const userPrompts = [
            {
                type: "input",
                name: "initialBranchOrHash",
                message: "Choose first branch or hash to check:",
                default: initBranch1
            },
            {
                type: "input",
                name: "branchOrHashToCheck",
                message: "Choose branch or hash to compare with initial:"
            },
            {
                type: "list",
                name: "commandToBuild",
                message: "Choose command to build with webpack-bundle-analyzer. WARNING: Current version compatible only with json as analyzerMode",
                choices: commands
            },
            {
                type: "confirm",
                name: "shouldInstallPackages",
                message: "Should packages be installed before each build?"
            }, 
        ];
        let MODE = "prompt";
        if (Object.keys($4fa36e821943b400$var$argv) && Object.keys($4fa36e821943b400$var$argv).length > 0) MODE = $4fa36e821943b400$var$MODES.inline;
        const compareSettings = MODE === $4fa36e821943b400$var$MODES.prompt ? await $gXNCa$inquirer.prompt(userPrompts) : await $4fa36e821943b400$require$getSettingsFromArgs(commands);
        console.log("compareSettings", compareSettings);
        const { initialBranchOrHash: initialBranchOrHash = initBranch1 , branchOrHashToCheck: branchOrHashToCheck , commandToBuild: commandToBuild , shouldInstallPackages: shouldInstallPackages ,  } = compareSettings;
        if (initBranch1 !== initialBranchOrHash) await $4fa36e821943b400$require$changeHEAD(initialBranchOrHash);
        const initialBuildResult = await $4fa36e821943b400$require$runBuild(commandToBuild, shouldInstallPackages);
        await $4fa36e821943b400$require$changeHEAD(branchOrHashToCheck);
        const buildResultToCompare = await $4fa36e821943b400$require$runBuild(commandToBuild, shouldInstallPackages);
        $4fa36e821943b400$require$showResultReport(initialBuildResult, buildResultToCompare);
    } catch (error) {
        console.error("Unhandled error", error);
    } finally{
        await $4fa36e821943b400$require$changeHEAD(initBranch);
    }
};
$4fa36e821943b400$var$runScript();


//# sourceMappingURL=index.js.map
