# git-bundle-check

Package for check [webpack-analyze-bundler](https://www.npmjs.com/package/webpack-bundle-analyzer) result through checking results for two commits in git repo.

## How to use

Go through this guide or check how it works in [test project](https://codeberg.org/vkomanchy/git-bundle-check-test)

### Install package

    npm install git-bundle-check

### Install dependencies

    npm install webpack-bundle-analyzer

### Configure build

Add webpack-bundle-analyzer to plugins in webpack config and set options to:

    {
      analyzerMode: 'json',
      reportFilename: '../bundle-sizes-info.json',
      openAnalyzer: false,
    }

WARNING! reportFilename always should be bundle-sizes-info.json

### Run package in "prompt" mode

Use `npx git-bundle-check` or add `git-bundle-check` as script to your package.json.

Run command and answer for prompts.

### Run package in "inline" (CLI) mode

Use `npx git-bundle-check -h` for full information about using package in inline mode.

